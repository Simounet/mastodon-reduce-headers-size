# Mastodon reduce headers size

## Why?

Mastodon cached files are getting bigger and bigger. This script replaces big profile's headers (>100ko) by small default files to keep your instance as light as possible.

## Warning!

This action is not reversible. Every file selected will be lost forever.

## How?

1. Clone this repository
2. Modify the `MASTODON_PATH` variable included at the top of `reduce.sh` to point to your Mastodon's installation path
3. Run `reduce.sh`

### Optional size preference

By default, files above 100ko will be replaced. You can modify the `SIZE_LIMIT` variable included at the top of `reduce.sh` if you prefer a lower or a bigger value.

## TODO

- extract `MASTODON_PATH` and `SIZE_LIMIT` variables from the file `reduce.sh`
