#! /bin/bash

MASTODON_PATH='/home/mastodon/live'
SIZE_LIMIT='+100k'

# DO NOT MODIFY BELOW THIS LINE
MASTODON_SYSTEM_DIR="${MASTODON_PATH}/public/system"
ACCOUNT_HEADERS_FOLDER="${MASTODON_SYSTEM_DIR}/accounts/headers/"
CACHE_HEADERS_FOLDER="${MASTODON_SYSTEM_DIR}/cache/accounts/headers/"

SCRIPT_PATH=`readlink -f "$0"`
IMAGES_FOLDER=`dirname "$SCRIPT_PATH"`
IMAGES_DEFAULT_BASE="${IMAGES_FOLDER}/images/default."

mastoreducer() {
	EXTENSION=$1
	find $ACCOUNT_HEADERS_FOLDER -name "*.${EXTENSION}" -size $SIZE_LIMIT -exec cp ${IMAGES_DEFAULT_BASE}${EXTENSION} {} \;
	find $CACHE_HEADERS_FOLDER -name "*.${EXTENSION}" -size $SIZE_LIMIT -exec cp ${IMAGES_DEFAULT_BASE}${EXTENSION} {} \;
}
mastoreducer png
mastoreducer gif
mastoreducer jpg
mastoreducer jpeg
